QT       += core gui opengl multimedia

TEMPLATE = app
TARGET = MyFFmpegPlayer
DESTDIR = bin

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32 {
LIBS += \
    -L$$PWD/lib/ffmpeg/lib \
    -lavcodec \
    -lavdevice \
    -lavfilter \
    -lavformat \
    -lavutil \
    -lswresample \
    -lswscale

INCLUDEPATH += src \
    lib/ffmpeg/include
}

unix {
LIBS += \
    -lavcodec \
    -lavdevice \
    -lavfilter \
    -lavformat \
    -lavutil \
    -lswresample \
    -lswscale
}

SOURCES += \
    src/MyVideoWidget.cpp \
    src/XAudioPlay.cpp \
    src/XFFMpeg.cpp \
    src/XVideoThread.cpp \
    src/main.cpp \
    src/MyFFmpegPlayer.cpp

HEADERS += \
    src/MyFFmpegPlayer.h \
    src/MyVideoWidget.h \
    src/XAudioPlay.h \
    src/XFFMpeg.h \
    src/XVideoThread.h

FORMS += \
    src/MyFFmpegPlayer.ui

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target
