﻿#ifndef XAUDIOPLAY_H
#define XAUDIOPLAY_H

#pragma once
#include <QAudioOutput>
#include <QMutex>


class XAudioPlay
{
public:
    QAudioOutput* _output = NULL;
    QIODevice* _io = NULL;
    QMutex _mutex;

public:
    int _sampleRate = 48000;
    int _sampleSize = 16;
    int _channel = 2;
    QString _codec = "audio/pcm";

    // XAudioPlay interface
public:
    static XAudioPlay* init();

    /** 设置播放的格式及参数
     * @brief start
     * @return
     */
    bool start() ;

    void stop() ;
    void play(bool isPlay) ;
    bool write(const char *data, int datasize) ;

    /** 这里在我们缓冲区写入时，我们先得保证缓冲区有空间让我们写入，所以这里的GetFree（）函数用来检测当前缓冲区大小。
     * @brief getFree
     * @return
     */
    int getFree() ;

private:
    XAudioPlay();
};

#endif // XAUDIOPLAY_H
