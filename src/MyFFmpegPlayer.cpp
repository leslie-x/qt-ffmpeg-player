﻿#include "MyFFmpegPlayer.h"
#include "ui_MyFFmpegPlayer.h"

#include "XFFMpeg.h"
#include "XVideoThread.h"
#include "XAudioPlay.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>

MyFFmpegPlayer::MyFFmpegPlayer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MyFFmpegPlayer)
{
    ui->setupUi(this);
}

MyFFmpegPlayer::~MyFFmpegPlayer()
{
    delete ui;
    XVideoThread::init()->setExit(true);
}

void MyFFmpegPlayer::open()
{
    QString name = QFileDialog::getOpenFileName(this,"select video","","*.mp4;;*.wmv;;*.gif");
    if(name.isEmpty())return;

    this->setWindowTitle(name);

    if( !XFFMpeg::init()->open(name.toLocal8Bit()) )
    {
        QMessageBox::warning(this,"error","file open failed!");
    }

    //初始化音频
    XAudioPlay::init()->_sampleRate = XFFMpeg::init()->_sampleRate;
    XAudioPlay::init()->_channel = XFFMpeg::init()->_channel;
    XAudioPlay::init()->_sampleSize = XFFMpeg::init()->_sampleSize;
    XAudioPlay::init()->start();
}


void MyFFmpegPlayer::on_pushButton_clicked()
{
    open();
}

