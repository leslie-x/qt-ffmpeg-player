﻿#ifndef MYVIDEOWIDGET_H
#define MYVIDEOWIDGET_H

#include <QOpenGLWidget>

class MyVideoWidget : public QOpenGLWidget
{
public:
    MyVideoWidget(QWidget* parent = NULL);

protected:
    /** 计时刷新
     * @brief timerEvent
     * @param event
     */
    void timerEvent(QTimerEvent *event) override;

    /** 绘制一帧图像
     * @brief paintGL
     */
    void paintGL() override;

private:
    QImage* _image{Q_NULLPTR};
};

#endif // MYVIDEOWIDGET_H
