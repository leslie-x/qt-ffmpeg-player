﻿#ifndef XVIDEOTHREAD_H
#define XVIDEOTHREAD_H

#include <QThread>

class AVFrame;

class XVideoThread : public QThread
{
public:
    static XVideoThread* init()
    {
        static XVideoThread vt;
        return &vt;
    }

    void setExit(bool isExit){_isExit = isExit;}

private:
    XVideoThread();

    bool _isExit {false};

    // QThread interface
protected:
    /** 线程进行读帧解码
     * @brief run
     */
    void run() override;
};

#endif // XVIDEOTHREAD_H
