﻿#ifndef MYFFMPEGPLAYER_H
#define MYFFMPEGPLAYER_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class MyFFmpegPlayer; }
QT_END_NAMESPACE

class MyFFmpegPlayer : public QWidget
{
    Q_OBJECT

public:
    MyFFmpegPlayer(QWidget *parent = nullptr);
    ~MyFFmpegPlayer();

    void open();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MyFFmpegPlayer *ui;
};
#endif // MYFFMPEGPLAYER_H
