﻿#include "MyVideoWidget.h"

#include "XFFMpeg.h"
#include "XVideoThread.h"

#include <QPainter>

MyVideoWidget::MyVideoWidget(QWidget* parent)
    :QOpenGLWidget(parent)
{
    startTimer(40);

    XVideoThread::init()->start();
}

void MyVideoWidget::timerEvent(QTimerEvent *event)
{
    this->update();
}

void MyVideoWidget::paintGL()
{
    if(!_image)
    {
        uchar* buf = new uchar[width()*height()*4]; //存放解码后的视频空间
        _image = new QImage(buf,width(),height(),QImage::Format_ARGB32);
    }

    //获取解码帧
    AVFrame* yuv = XFFMpeg::init()->getVideoFrame();
    {
        if(!yuv) return;
    }

    //转码帧
    XFFMpeg::init()->toRGB(yuv,(char*)_image->bits(),width(),height());

    QPainter painter;
    painter.begin(this);
    painter.drawImage(QPoint(0,0),*_image);
    painter.end();
}
