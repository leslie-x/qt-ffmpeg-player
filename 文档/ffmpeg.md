# ffmpeg

帮助文档：[ffmpeg Documentation](http://ffmpeg.org/ffmpeg.html)

```
char* path = ppath.toLatin1().data();//QString转char*
```

## 流程图

![image-20220405233522970](ffmpeg.assets/image-20220405233522970.png)

## FFmpeg解码函数简介

▫ av_register_all()：注册所有组件。

▫ avformat_open_input()：打开输入视频文件。

▫ avformat_find_stream_info()：获取视频文件信息。

▫ avcodec_find_decoder()：查找解码器。

▫ avcodec_open2()：打开解码器。

▫ av_read_frame()：从输入文件读取一帧压缩数据。

▫ avcodec_decode_video2()：解码一帧压缩数据。

▫ avcodec_close()：关闭解码器。

▫ avformat_close_input()：关闭输入视频文件。





## FFmpeg数据结构简介

▫ AVFormatContext

 封装格式上下文结构体，也是统领全局的结构体，保存了视频文件封装

格式相关信息。

▫ AVInputFormat

 每种封装格式（例如FLV, MKV, MP4, AVI）对应一个该结构体。

▫ AVStream

 视频文件中每个视频（音频）流对应一个该结构体。

▫ AVCodecContext

 编码器上下文结构体，保存了视频（音频）编解码相关信息。

▫ AVCodec

 每种视频（音频）编解码器(例如H.264解码器)对应一个该结构体。

▫ AVPacket

 存储一帧压缩编码数据。

▫ AVFrame

 存储一帧解码后像素（采样）数据。

## 流程

```cpp
#include "src/MyFFmpegPlayer.h"

#include <QApplication>
#include <QDebug>

extern "C"{//必须
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
}

int main(int argc, char *argv[])
{

    //打开视频获取视频信息
    //1.av_register_all()：注册所有组件。
    av_register_all();

    //2.avformat_open_input()：打开输入视频文件。
    QString ppath = "1234.mp4";
    char* path = ppath.toLatin1().data();//QString转char*

    AVFormatContext* ic = Q_NULLPTR;
    int re = avformat_open_input(&ic,path,0,0);

    if(re!=0)
    {//0为成功，非0打开错误
        char buf[1024] = {0};
        av_strerror(re,buf,sizeof (buf));
        qDebug()<<buf;
        return -1;
    }
    //读取信息 时间长度
    int totalSec = ic->duration/(AV_TIME_BASE);
    qDebug()<<QString("file totalsec is %1:%2").arg(totalSec/60).arg(totalSec%60);

    //3.打开视频解码器
    int videoStream = 0;
    for(int i=0; i < ic->nb_streams; ++i)
    {
        AVCodecContext *enc = ic->streams[i]->codec;//解码上下文
        if(enc->codec_type==AVMEDIA_TYPE_VIDEO)
        {//判断为视频
            videoStream = i;
            AVCodec *codec = avcodec_find_decoder(enc->codec_id);//查找解码器
            if(!codec){
                qDebug()<<"open codec failed!";
                return -1;
            }
            int err = avcodec_open2(enc,codec,NULL);
            if(err!=0){//未打开解码器
                char buf[1024] = {0};
                av_strerror(re,buf,sizeof (buf));
                qDebug()<<buf;
                return -1;
            }
            qDebug()<<"open codec success!"<<"videostream:"<<videoStream;
        }
    }

    //4.av_read_frame:读取视频分析视频包
    SwsContext* cCtx{Q_NULLPTR};
    AVCodecContext* videoCtx = ic->streams[videoStream]->codec;
    AVFrame *yuv = av_frame_alloc();//AVFrame存储一帧解码后像素（采样）数据
    //视频解码并分析H264解码
    for(;;){
        AVPacket pkt;//存储一帧压缩编码数据。
        re = av_read_frame(ic,&pkt);//从输入文件读取一帧压缩数据。
        if(re!=0)break;
        if(pkt.stream_index != videoStream) continue;

        int pts = av_rescale_q(pkt.pts,videoCtx->time_base,ic->streams[pkt.stream_index]->time_base);
        int got_picture = 0;

        //5.avcodec_decode_video2()：解码一帧压缩数据
//        int re = avcodec_decode_video2(videoCtx,yuv,&got_picture,&pkt);
//        if(got_picture){
//            qDebug()<<re;
//        }
        //新版
        int re = avcodec_send_packet(videoCtx ,&pkt);//发送视频帧
        if (re != 0)
        {
            av_packet_unref(&pkt);//不成功就释放这个pkt
            continue;
        }
        re = avcodec_receive_frame(videoCtx, yuv);//接受后对视频帧进行解码
        if (re != 0)
        {
            av_packet_unref(&pkt);//不成功就释放这个pkt
            continue;
        }
        qDebug()<<"D";
        //

        //转码YUV->RGB
        cCtx = sws_getCachedContext(cCtx,videoCtx->width,\
                                    videoCtx->height,
                                    videoCtx->pix_fmt,//输入格式
                                    outwidth,outheight,
                                    AV_PIX_FMT_BGRA,//输出格式
                                    SWS_BICUBIC,//转码算法
                                    NULL,NULL,NULL);
        if(!cCtx){
             qDebug()<<"sws_getCachedContext  failed!";
        }
        uint8_t *data[AV_NUM_DATA_POINTERS] = { 0 };
	data[0] = (uint8_t *)out;//第一位输出RGB
	int linesize[AV_NUM_DATA_POINTERS] = { 0 };
 
	linesize[0] = outwidth * 4;//一行的宽度，32位4个字节
	int h = sws_scale(cCtx, yuv->data, //当前处理区域的每个通道数据指针
		yuv->linesize,//每个通道行字节数
		0, videoCtx->height,//原视频帧的高度
		data,//输出的每个通道数据指针	
		linesize//每个通道行字节数
 
		);//开始转码
 
	if (h > 0)
	{
		printf("(%d)", h);
    }
        qDebug()<<QString("pts = %1").arg(pkt.pts);//打印它的pts,用来显示的时间戳，后面用来做同步
        av_packet_unref(&pkt);//擦除读帧，重新置零
    }
    
    if (cCtx)
    {
        sws_freeContext(cCtx);
        cCtx = NULL;
    }

    //释放解封装空间
    avformat_close_input(&ic);

    QApplication a(argc, argv);
    MyFFmpegPlayer w;
    w.show();
    return a.exec();

}

```

## 封装

.h

```cpp
#ifndef MYFFMPEGPLAYER_H
#define MYFFMPEGPLAYER_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class MyFFmpegPlayer; }
QT_END_NAMESPACE

class MyFFmpegPlayer : public QWidget
{
    Q_OBJECT

public:
    MyFFmpegPlayer(QWidget *parent = nullptr);
    ~MyFFmpegPlayer();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MyFFmpegPlayer *ui;
    int _outheight=800,_outwidth=600;
    char *rgb{Q_NULLPTR};
};
#endif // MYFFMPEGPLAYER_H

```

.cpp

```cpp
#include "MyFFmpegPlayer.h"
#include "ui_MyFFmpegPlayer.h"

#include "XFFMpeg.h"
#include <QDebug>

MyFFmpegPlayer::MyFFmpegPlayer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MyFFmpegPlayer)
{
    ui->setupUi(this);
    rgb = new char[_outheight*_outwidth*4];
}

MyFFmpegPlayer::~MyFFmpegPlayer()
{
    delete ui;
}


void MyFFmpegPlayer::on_pushButton_clicked()
{
    if(XFFMpeg::init()->open("1234.mp4")){
        qDebug()<<"open success!";
    }else{
        qDebug()<<XFFMpeg::init()->getError().c_str();
    }

    for(;;)
    {
        AVPacket pkt = XFFMpeg::init()->read();//获取视频帧
        if(pkt.size==0)break;

        if(pkt.stream_index != XFFMpeg::init()->getVideoStream()){//判断不为视频流
            av_packet_unref(&pkt);
            continue;
        }
        qDebug()<<"pts="<<pkt.pts;

        AVFrame* yuv = XFFMpeg::init()->decode(&pkt);//解码视频帧
        if(yuv){
            qDebug()<<"D";
            XFFMpeg::init()->toRGB(yuv,rgb,800,600);
        }

        av_packet_unref(&pkt);
    }
}


```

use

```cpp
void MyFFmpegPlayer::on_pushButton_clicked()
{
    if(XFFMpeg::init()->open("1234.mp4")){
        qDebug()<<"open success!";
    }else{
        qDebug()<<XFFMpeg::init()->getError().c_str();
    }

    for(;;)
    {
        AVPacket pkt = XFFMpeg::init()->read();//获取视频帧
        if(pkt.size==0)break;

        if(pkt.stream_index != XFFMpeg::init()->getVideoStream()){//判断不为视频流
            av_packet_unref(&pkt);
            continue;
        }
        qDebug()<<"pts="<<pkt.pts;

        AVFrame* yuv = XFFMpeg::init()->decode(&pkt);//解码视频帧
        if(yuv){
            qDebug()<<"D";
            XFFMpeg::init()->toRGB(yuv,rgb,800,600);
        }

        av_packet_unref(&pkt);
    }
}
```

```
open codec success!
"file totalsec is 0:12"
open success!
pts= 0
pts= 2048
pts= 1024
D
600
pts= 512
D
600
```

## QOpenGL显示

video.h

```cpp
#ifndef MYVIDEOWIDGET_H
#define MYVIDEOWIDGET_H

#include <QOpenGLWidget>

class MyVideoWidget : public QOpenGLWidget
{
public:
    MyVideoWidget(QWidget* parent = NULL);

    // QObject interface
protected:
    /** 计时刷新
     * @brief timerEvent
     * @param event
     */
    void timerEvent(QTimerEvent *event) override;

    // QWidget interface
protected:
    /** 绘制一帧图像
     * @brief paintEvent
     * @param event
     */
    void paintEvent(QPaintEvent *event) override;

private:
    QImage* _image{Q_NULLPTR};
};

#endif // MYVIDEOWIDGET_H

```

video.cpp

```cpp
#include "MyVideoWidget.h"

#include "XFFMpeg.h"
#include <QPainter>

MyVideoWidget::MyVideoWidget(QWidget* parent)
    :QOpenGLWidget(parent)
{
    XFFMpeg::init()->open("1234.mp4");
    startTimer(10);
}

void MyVideoWidget::timerEvent(QTimerEvent *event)
{
    this->update();
}

void MyVideoWidget::paintEvent(QPaintEvent *event)
{
    if(!_image)
    {
        uchar* buf = new uchar[width()*height()*4]; //存放解码后的视频空间
        _image = new QImage(buf,width(),height(),QImage::Format_ARGB32);
    }

    //读取视频帧
    AVPacket pkt = XFFMpeg::init()->read();
    {
        if(pkt.stream_index != XFFMpeg::init()->getVideoStream()){
            av_packet_unref(&pkt);
            return;
        }
        if(pkt.size==0) return;
    }

    //解码帧
    AVFrame* yuv = XFFMpeg::init()->decode(&pkt);
    {
        av_packet_unref(&pkt);//释放解码空间
        if(!yuv) return;
    }

    //转码帧
    XFFMpeg::init()->toRGB(yuv,(char*)_image->bits(),width(),height());

    QPainter painter;
    painter.begin(this);
    painter.drawImage(QPoint(0,0),*_image);
    painter.end();

}

```

## 控制条进度

如何控制视频播放及进度的控制，内容主要分为以下几个部分

1、创建解码线程控制播放速度

2、通过Qt打开外部视频

3、视频总时间显示和播放的当前时间显示

4、进度条显示播放进度、拖动进度条控制播放位置

5、控制视频播放和暂停

6、视频显示和窗口大小变化同步

7、重载Qt滑动条类鼠标点击移动滑动条并跳转到相应视频位置

videoThread.h

```cpp
#ifndef XVIDEOTHREAD_H
#define XVIDEOTHREAD_H

#include <QThread>

class AVFrame;

class XVideoThread : public QThread
{
public:
    static XVideoThread* init()
    {
        static XVideoThread vt;
        return &vt;
    }

    void setExit(bool isExit){_isExit = isExit;}

    AVFrame* getOneFrame(){return _yuv;}

private:
    XVideoThread();

    bool _isExit {false};
    AVFrame* _yuv{Q_NULLPTR};

    // QThread interface
protected:
    /** 线程进行读帧解码
     * @brief run
     */
    void run() override;
};

#endif // XVIDEOTHREAD_H

```

videoThread.cpp

```cpp
#include "XVideoThread.h"

#include "XFFMpeg.h"

XVideoThread::XVideoThread()
{

}

void XVideoThread::run()
{
   while(!_isExit)
   {
       //读取视频帧
       AVPacket pkt = XFFMpeg::init()->read();
       {
           if(pkt.size <= 0)//未打开视频
           {
               //读取AVPacket包，若未打开视频，此时pkt.size必然小于0，线程睡眠一段时间继续。否则我们开始解码视频帧，同时利用线程的睡眠时间控制解码进度进而来控制播放的速度。
               msleep(10);
               continue;
           }
           if(pkt.stream_index != XFFMpeg::init()->getVideoStream())
           {
               av_packet_unref(&pkt);
               continue;
           }
       }

       //解码帧
       _yuv = XFFMpeg::init()->decode(&pkt);
       {
           av_packet_unref(&pkt);//释放解码空间

           if(XFFMpeg::init()->getFPS() > 0){//控制解码的进度
               msleep(1000 / XFFMpeg::init()->getFPS());
           }
       }
   }
}

```



# 音频

对于音频部分，主要从以下几个部分实现。

1、音频播放的启动、停止、暂停、缓冲写入接口实现

2、[ffmpeg](https://so.csdn.net/so/search?q=ffmpeg&spm=1001.2101.3001.7020)音频解码器打开和音频解码

3、ffmpeg音频[重采样](https://so.csdn.net/so/search?q=重采样&spm=1001.2101.3001.7020)标准化音频的输出格式

4、[多线程](https://so.csdn.net/so/search?q=多线程&spm=1001.2101.3001.7020)和缓冲队列实现音视频同步播放



```cpp
//判断为音频流
            else if(enc->codec_type==AVMEDIA_TYPE_AUDIO)
            {
                _audioStream = i;
                AVCodec *codec = avcodec_find_decoder(enc->codec_id);//查找解码器
                if( avcodec_open2(enc,codec,NULL) < 0 ) return 0; //使用解码器打开封装

                _sampleRate = enc->sample_rate;
                _channel = enc->channels;
                switch (enc->sample_fmt) {
                case AV_SAMPLE_FMT_S16:
                    _sampleSize = 16;
                    break;
                case AV_SAMPLE_FMT_S32:
                    _sampleSize = 32;
                    break;
                default:
                    break;
                }
                qDebug()<<QString("audio sampleRate: %1, sampleSize: %2, channels: %3").arg(_sampleRate).arg(_sampleSize).arg(_channel);
            }
```

解码

```cpp
int XFFMpeg::decode(const AVPacket *pkt)
{
    QMutexLocker lock(&_mutex);

    if(!_ic) return NULL;

    //申请解码的对象空间
    if(!_yuv) _yuv = av_frame_alloc();
    if(!_pcm) _pcm = av_frame_alloc();

    AVFrame* frame = _yuv;
    if(pkt->stream_index == _audioStream) frame = _pcm;

    //发送之前读取的视频帧pkt
    int re = avcodec_send_packet(_ic->streams[pkt->stream_index]->codec,pkt);
    if(re!=0){return NULL;}
    //解码pkt后存入frame中
    re = avcodec_receive_frame(_ic->streams[pkt->stream_index]->codec,frame);
    if(re!=0){return NULL;}

    qDebug()<<"pts= "<<frame->pts;

    int p = frame->pts * r2d(_ic->streams[pkt->stream_index]->time_base);
    if(pkt->stream_index == _audioStream) _pts = p;
    return p;
}
```

采样

```cpp
int XFFMpeg::toPCM(char *out)
{
    QMutexLocker lock(&_mutex);
    if(!_ic || !_pcm || !out) return 0;

    AVCodecContext* audioCtx = _ic->streams[_audioStream]->codec;//音频解码器封装
    if(!_aCtx)
    {
        _aCtx = swr_alloc();
        swr_alloc_set_opts( _aCtx, audioCtx->channel_layout,
                            AV_SAMPLE_FMT_S16,
                            audioCtx->sample_rate,
                            audioCtx->channels,
                            audioCtx->sample_fmt,
                            audioCtx->sample_rate,
                            0,0
                            );
        swr_init(_aCtx);
    }
    uint8_t *data[1];
    data[0] = (uint8_t*)out;

    //音频的重采样过程
    //return number of samples output per channel, negative value on error
    int len = swr_convert(_aCtx,data,
                          10000,
                          (const uint8_t**)_pcm->data,
                          _pcm->nb_samples
                          );
    if(len<=0) return 0;

    //计算音频占用的字节数的
    int outsize = av_samples_get_buffer_size(NULL,audioCtx->channels,
                                             _pcm->nb_samples,
                                             AV_SAMPLE_FMT_S16,
                                             0);
    return outsize;

}
```

修改线程

```cpp
void XVideoThread::run()
{
    char out[10000] = {0};
    while(!_isExit)
    {
        //判断暂停状态

        //缓冲区
        if(XFFMpeg::init()->getAudioStream()!=-1){
            int free = XAudioPlay::init()->getFree();
            if(free < 10000)
            {
                msleep(1);
                continue;
            }
        }

        //读取视频帧
        AVPacket pkt = XFFMpeg::init()->read();

        if(pkt.size <= 0)//未打开视频
        {
            //读取AVPacket包，若未打开视频，此时pkt.size必然小于0，线程睡眠一段时间继续。否则我们开始解码视频帧，同时利用线程的睡眠时间控制解码进度进而来控制播放的速度。
            msleep(10);
            continue;
        }

        if(XFFMpeg::init()->getAudioStream()!=-1){
            //音频优先
            if(pkt.stream_index == XFFMpeg::init()->getAudioStream())
            {
                XFFMpeg::init()->decode(&pkt);
                av_packet_unref(&pkt);
                int len = XFFMpeg::init()->toPCM(out);
                XAudioPlay::init()->write(out,len);
                continue;
            }
        }else{

            if(pkt.stream_index != XFFMpeg::init()->getVideoStream())
            {
                av_packet_unref(&pkt);
                continue;
            }
        }

        //解码帧
        XFFMpeg::init()->decode(&pkt);
        {
            av_packet_unref(&pkt);//释放解码空间

            if(XFFMpeg::init()->getAudioStream()==-1){//不包含音频
                if(XFFMpeg::init()->getFPS() > 0){//控制解码的进度
                    msleep(1000 / XFFMpeg::init()->getFPS());
                }
            }
        }
    }
}
```

