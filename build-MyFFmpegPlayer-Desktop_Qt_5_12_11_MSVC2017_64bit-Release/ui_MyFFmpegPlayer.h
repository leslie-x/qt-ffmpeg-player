/********************************************************************************
** Form generated from reading UI file 'MyFFmpegPlayer.ui'
**
** Created by: Qt User Interface Compiler version 5.12.11
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYFFMPEGPLAYER_H
#define UI_MYFFMPEGPLAYER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>
#include "MyVideoWidget.h"

QT_BEGIN_NAMESPACE

class Ui_MyFFmpegPlayer
{
public:
    QGridLayout *gridLayout;
    MyVideoWidget *openGLWidget;
    QHBoxLayout *horizontalLayout_2;
    QSlider *horizontalSlider;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton;

    void setupUi(QWidget *MyFFmpegPlayer)
    {
        if (MyFFmpegPlayer->objectName().isEmpty())
            MyFFmpegPlayer->setObjectName(QString::fromUtf8("MyFFmpegPlayer"));
        MyFFmpegPlayer->resize(689, 589);
        gridLayout = new QGridLayout(MyFFmpegPlayer);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        openGLWidget = new MyVideoWidget(MyFFmpegPlayer);
        openGLWidget->setObjectName(QString::fromUtf8("openGLWidget"));

        gridLayout->addWidget(openGLWidget, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSlider = new QSlider(MyFFmpegPlayer);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider);

        label = new QLabel(MyFFmpegPlayer);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(label);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_2 = new QPushButton(MyFFmpegPlayer);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton = new QPushButton(MyFFmpegPlayer);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);


        retranslateUi(MyFFmpegPlayer);

        QMetaObject::connectSlotsByName(MyFFmpegPlayer);
    } // setupUi

    void retranslateUi(QWidget *MyFFmpegPlayer)
    {
        MyFFmpegPlayer->setWindowTitle(QApplication::translate("MyFFmpegPlayer", "MyFFmpegPlayer", nullptr));
        label->setText(QApplication::translate("MyFFmpegPlayer", "TextLabel", nullptr));
        pushButton_2->setText(QApplication::translate("MyFFmpegPlayer", "stop", nullptr));
        pushButton->setText(QApplication::translate("MyFFmpegPlayer", "play", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MyFFmpegPlayer: public Ui_MyFFmpegPlayer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYFFMPEGPLAYER_H
